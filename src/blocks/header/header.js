jQuery(document).ready(function($) {
  
  var menu = $('.header__right')
  var sandwich = $('.js-sandwich-open')
  var sandwichClose = $('.js-sandwich-close')
  
  var menuCloseHandler = function() {
    menu.addClass('is-hidden').delay(300).queue(function (next) {
      menu.removeClass('is-active')
      menu.removeClass('is-hidden')
      next()
    })
  }
  var menuOpenHandler = function(){
    menu.addClass('is-active');
  }

  sandwich.on('click', function (e) {
    e.preventDefault()
    menuOpenHandler()
  })
  sandwichClose.on('click', function (e) {
    e.preventDefault()
    menuCloseHandler()
  })
  var KEYCODE_ESC = 27
  $(document).keyup(function (e) {
    if (e.keyCode === KEYCODE_ESC) {
      menuCloseHandler()
    }
  })
  
})
