$(document).ready(function () {
    $('.about-slider').slick({
        centerMode: true,
        centerPadding: '19%',
        arrows: false,
        slidesToShow: 3,
        focusOnSelect: true,
        dots: false,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 3000,
        pauseOnFocus: false,
        pauseOnHover: false,
        infinite: true,
        responsive: [
          {
            breakpoint: 1200,
            settings: {
                centerPadding: '17%',
            }
          },
          {
            breakpoint: 992,
            settings: {
                centerPadding: '30%',
                slidesToShow: 1
            }
          },
          {
            breakpoint: 768,
            settings: {
                centerPadding: '25%',
                slidesToShow: 1
            }
          },
          {
            breakpoint: 576,
            settings: {
                centerPadding: '15%',
                slidesToShow: 1
            }
          }
        ]
      });
      $('.about-slider').on('beforeChange', function(event, slick, currentSlide, nextSlide){
        let slidesLength = slick.$slides.length - 1,
            isCurrentFirstOrLast = currentSlide === 0 || currentSlide === slidesLength,
            isNextFirstOrLast = nextSlide === 0 || nextSlide === slidesLength;
            
        if (isCurrentFirstOrLast && isNextFirstOrLast){
          let nextClone = $(event.currentTarget).find('.slick-cloned.slick-active');
          setTimeout(function(){
            nextClone.addClass('slick-current');
          }, 100)
        }
      });
});



 